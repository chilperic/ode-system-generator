from typing import Callable
from dataclasses import dataclass


@dataclass
class Moiety:
    func: Callable
    args: list[str]
    derived_variable: str


@dataclass
class Rate:
    func: Callable
    args: list[str]
