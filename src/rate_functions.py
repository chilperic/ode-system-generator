def competitor(s: float, ksi: float, p: float, kpi: float) -> float:
    return s / ksi + p / kpi


def competitors(competitors: list[list[float]]) -> float:
    return sum(competitor(*args) for args in competitors)


def inhibitor(i: float, ki: float, n: float = 1.0) -> float:
    return (i / ki) ** n


def inhibitors(inhibitors: list[list[float]]) -> float:
    return sum(inhibitor(*args) for args in inhibitors)
