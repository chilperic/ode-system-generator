from typing import Any


def invert_stoichiometries(
    stoichiometries_by_rate: dict[str, dict[str, float]]
) -> dict[str, dict[str, float]]:
    stoichiometries_by_compounds: dict[str, dict[str, float]] = {}
    for rate_name, stoichiometry in stoichiometries_by_rate.items():
        for compound, factor in stoichiometry.items():
            stoichiometries_by_compounds.setdefault(compound, {})[rate_name] = factor
    return stoichiometries_by_compounds


def flatten(iterable: list[list[Any]]) -> list[Any]:
    return [j for i in iterable for j in i]


def stringify_nested_list(nested_list: list[list[str]]) -> str:
    intermediate = [f"[{', '.join(list_)}]" for list_ in nested_list]
    return f"[{', '.join(intermediate)}]"


def unpack_stoichiometries(stoichiometries: dict[str, float]) -> tuple[list[str], list[str]]:
    substrates = []
    products = []
    for cpd, val in stoichiometries.items():
        if val < 0:
            substrates.append(cpd)
        else:
            products.append(cpd)
    return substrates, products


def head(d: dict, n: int) -> dict:
    return dict(list(d.items())[:n])
